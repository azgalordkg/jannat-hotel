import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from "./containers/Home/Home";

import './App.css';
import RegencyResort from "./containers/RegencyResort/RegencyResort";

class App extends Component {
  render() {
    return (
      <Switch>
        <Route path="/" exact component={Home}/>
        <Route path="/regency" exact component={RegencyResort}/>
        <Route path="/resort" exact component={RegencyResort}/>
      </Switch>
    );
  }
}

export default App;
