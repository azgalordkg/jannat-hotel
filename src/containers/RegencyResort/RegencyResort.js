import React, {Component} from 'react';
import Layout from "../../components/Layout/Layout";
import {connect} from "react-redux";
import {fetchRegencyResortData} from "../../store/actions/regencyResort";
import RegResMain from "../../components/RegencyResortComponents/RegResMain/RegResMain";

class RegencyResort extends Component {
  state = {
    startDate: new Date(),
    endDate: new Date(),
    regency: true,
    navLinks: {
      regency: [
        {id: 'about', text: 'О нас'},
        {id: 'rooms', dropDowns: {}, text: 'НОМЕРА'},
        {id: 'health', dropDowns: {}, text: 'ЗДОРОВЬЕ И ОТДЫХ'},
        {id: 'events', dropDowns: {}, text: 'МЕРОПРИЯТИЯ'},
        {id: 'restraints', dropDowns: {}, text: 'РЕСТОРАНЫ И БАРЫ'},
        {id: 'contacts', text: 'Контакты'},
      ],
      resort: [
        {id: 'about'},
        {id: 'rooms', dropDowns: {}, text: 'ЗДОРОВЬЕ И ОТДЫХ'},
        {id: 'health', dropDowns: {}, text: 'ЗДОРОВЬЕ И ОТДЫХ'},
        {id: 'events', dropDowns: {}, text: 'ЗДОРОВЬЕ И ОТДЫХ'},
        {id: 'restraints', dropDowns: {}, text: 'ЗДОРОВЬЕ И ОТДЫХ'},
        {id: 'contacts'},
      ]
    },
    languages: [
      {text: 'Ru', isActive: true},
      {text: 'En', isActive: false},
    ],
    mainForm: {
      ru: {
        select: {}
      },
      en: {},
      ifDrop: false,
    }
  };

  componentDidMount() {
    this.props.fetchRegencyResortData('ru');
  }

  // Functions for HomeBooking

  selectChange = (event) => {
    this.setState({selectValue: event.target.value});
  };

  changeStartDate = (date) => {
    this.setState({
      startDate: date
    });
  };

  changeEndDate = (date) => {
    this.setState({
      endDate: date
    });
  };

  // Functions for RegResMainSelect

  changeIfDrop = () => {
    this.setState({mainForm: {
      ...this.state.mainForm,
      ifDrop: !this.state.mainForm.ifDrop,
    }})
  };

  render() {
    if (!this.props.about_us) {
      return (<div>Loading...</div>)
    }

    return (
      <Layout
        navLinks={this.state.regency ? this.state.navLinks.regency : this.state.navLinks.resort}
        languages={this.state.languages} mainLogo={null}
      >
        <div className="RegencyResort">
          <RegResMain
            slideItems={this.props.slider}

            startTitle="Заезд"
            endTitle="Выезд"
            startSelected={this.state.startDate}
            startOnChange={this.changeStartDate}
            endSelected={this.state.endDate}
            endOnChange={this.changeEndDate}

            selectTitle="Гости"
            onSelectClick={this.changeIfDrop}
            guestsInfo="Количество гостей"
            ifDrop={this.state.mainForm.ifDrop}
            onAddClick={null}
            addButtonText={null}
            okClickText={null}
            cancelClickText={null}
          />
        </div>
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  about_us: state.regencyResort.about_us,
  slider: state.regencyResort.slider,
});

const mapDispatchToProps = dispatch => ({
  fetchRegencyResortData: lang => dispatch(fetchRegencyResortData(lang)),
});

export default connect(mapStateToProps, mapDispatchToProps)(RegencyResort);
