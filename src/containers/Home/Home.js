import React, {Component} from 'react';
import {connect} from 'react-redux';

import HomeHeader from "../../components/HomeComponents/HomeHeader/HomeHeader";
import HomeBooking from "../../components/HomeComponents/HomeBooking/HomeBooking";
import HomeAdvantages from "../../components/HomeComponents/HomeAdvantages/HomeAdvantages";
import HomeNews from "../../components/HomeComponents/HomeNews/HomeNews";
import HomeReasons from "../../components/HomeComponents/HomeReasons/HomeReasons";
import HomeReviews from "../../components/HomeComponents/HomeReviews/HomeReviews";
import HomeMap from "../../components/HomeComponents/HomeMap/HomeMap";
import HomeFooter from "../../components/HomeComponents/HomeFooter/HomeFooter";

import './Home.css';

import TripLogo from '../../assets/img/svg/trip.svg';

import {fetchHomeData} from "../../store/actions/home";

class Home extends Component {
  state = {
    selectValue: '',
    startDate: new Date(),
    endDate: new Date(),
    guests: 0,
    opened: false,
    footerTitles: [
      {
        0: 'Jannat Regency',
        1: 'Отдел бронирования',
        2: 'Банкетная служба',
      },
      {
        0: 'JANNAT RESORT',
        1: 'ОТДЕЛ БРОНИРОВАНИЯ',
        2: 'БАНКЕТНАЯ СЛУЖБА',
      },
      {
        0: 'ОТДЕЛ ПО РАБОТЕ С КОРПОРАТИВНЫМИ КЛИЕНТАМИ',
        1: '',
        2: 'БАНКЕТНАЯ СЛУЖБА',
      }
    ]
  };

  componentDidMount() {
    this.changeStartDate = this.changeStartDate.bind(this);
    this.changeEndDate = this.changeEndDate.bind(this);

    this.props.fetchHomeData('ru');
  }

  // Functions for HomeBooking

  selectChange = (event) => {
    this.setState({selectValue: event.target.value});
  };

  changeStartDate = (date) => {
    this.setState({
      startDate: date
    });
  };

  changeEndDate = (date) => {
    this.setState({
      endDate: date
    });
  };

  incrementGuests = () => {
    this.setState({guests: this.state.guests + 1});
  };

  decrementGuests = () => {
    if (this.state.guests) {
      this.setState({guests: this.state.guests - 1});
    }
  };

  // Functions for News

  getMountOfTextSymbols = text => {
    const mount = 152;
    if (text.length > mount) {
      const textArray = text.split('');
      let changedText = [];

      for (let i = 0; i < mount; i++) {
        changedText.push(textArray[i]);
      }
      changedText = changedText.join('') + '...';
      return changedText;
    }
    return text;
  };

  getMountOfTitleSymbols = title => {
    const mount = 37;
    if (title.length > mount) {
      const textArray = title.split('');
      let changedText = [];

      for (let i = 0; i < mount; i++) {
        changedText.push(textArray[i]);
      }
      changedText = changedText.join('') + '...';
      return changedText;
    }
    return title;
  };

  // Functions for reasons

  openText = () => {
    this.setState({opened: !this.state.opened});
  };

  render() {
    if (!this.props.banner) {
      return (<div>Loading...</div>)
    }

    return (
      <div id="top" className="Home">
        <HomeHeader links={this.props.banner}/>
        <HomeBooking
          selectValue={this.state.selectValue}
          selectChange={this.selectChange}
          dateStartSelected={this.state.startDate}
          dateStartChanged={this.changeStartDate}
          dateEndSelected={this.state.endDate}
          dateEndChanged={this.changeEndDate}
          guests={this.state.guests}
          increment={this.incrementGuests}
          decrement={this.decrementGuests}
        />
        <HomeAdvantages
          title='ПЯТИЗВЕЗДОЧНЫЕ ОТЕЛИ "JANNAT", КЫРГЫЗСТАН'
          items={this.props.services}
        />
        <HomeNews
          items={this.props.news}
          textSymbols={(text) => this.getMountOfTextSymbols(text)}
          titleSymbols={(title) => this.getMountOfTitleSymbols(title)}
        />
        <HomeReasons
          title={this.props.reasons[0].title}
          text={this.props.reasons[0].description}
          open={this.openText}
          height={this.state.opened}
          blockBackground={this.props.reasons[0].image}
        />
        <HomeReviews
          title="Посмотреть отзывы об отелях на"
          links={this.props.tripadvisor}
          titleLogo={TripLogo}
        />
        <HomeMap/>
        <HomeFooter
          titles={this.state.footerTitles}
          columns={this.props.contacts}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  banner: state.home.banner,
  contacts: state.home.contacts,
  reasons: state.home.reasons,
  news: state.home.news,
  services: state.home.services,
  tripadvisor: state.home.tripadvisor,
});

const mapDispatchToProps = dispatch => ({
  fetchHomeData: (lang) => dispatch(fetchHomeData(lang)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
