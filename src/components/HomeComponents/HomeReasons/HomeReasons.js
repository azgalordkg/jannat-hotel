import React from 'react';
import HomeReasonsTextBlock from "./HomeReasonsTextBlock/HomeReasonsTextBlock";
import Container from "../../Container/Container";

import './HomeReasons.css';

const imagesUrl = 'http://jannat.sunrisetest.site';

const HomeReasons = props => {
  return (
    <div style={{backgroundImage: `url(${imagesUrl + props.blockBackground})`}} className="HomeReasons">
      <Container>
        <HomeReasonsTextBlock
          title={props.title}
          open={props.open}
          height={props.height}
          text={props.text}
        />
      </Container>
    </div>
  );
};

export default HomeReasons;
