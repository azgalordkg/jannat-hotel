import React from 'react';

import './HomeReasonsTextBlock.css';

const HomeReasonsTextBlock = ({title, open, height, text}) => {
  return (
    <div className="HomeReasonsTextBlock">
      <h2>{title}</h2>
      <div
        style={{height: height ? 'auto' : '145px', marginBottom: height ? '20px' : '50px'}}
        className="HomeReasonsTextBlockContent"
        dangerouslySetInnerHTML={{__html: text}}
      />
      <button className={height ? 'active' : null} onClick={open}>{height ? 'Свернуть' : 'Развернуть'}</button>
    </div>
  );
};

export default HomeReasonsTextBlock;
