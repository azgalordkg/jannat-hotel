import React from 'react';
import HomeNewsSlider from "./HomeNewsSlider/HomeNewsSlider";
import HomeNewsSliderItem from "./HomeNewsSliderItem/HomeNewsSliderItem";

import './HomeNews.css';
import Container from "../../Container/Container";
const imagesUrl = 'http://jannat.sunrisetest.site';

const HomeNews = ({items, textSymbols, titleSymbols}) => {
  return (
    <div className="HomeNews">
      <Container>
        <HomeNewsSlider>
          {items.map((item, index) => (
            <HomeNewsSliderItem
              image={imagesUrl + item.image}
              title={titleSymbols(item.title)}
              text={textSymbols(item.description)}
              key={index}
            />
          ))}
        </HomeNewsSlider>
      </Container>
    </div>
  );
};

export default HomeNews;

