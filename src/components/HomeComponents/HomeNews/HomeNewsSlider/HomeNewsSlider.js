import React from 'react';

import './HomeNewsSlider.css';

const HomeNewsSlider = ({children}) => {
  return (
    <div uk-slider="true" className="HomeNewsSlider">
      <div className="uk-slider-container uk-light">
        <ul className="uk-slider-items uk-grid uk-child-width-1-3">
          {children}
        </ul>
      </div>
      <div className="HomeNewsSliderButtons">
        <button className="uk-position-center-left-out uk-position-small" uk-slidenav-previous="true"
           uk-slider-item="previous"/>
        <button className="uk-position-center-right-out uk-position-small" uk-slidenav-next="true"
           uk-slider-item="next"/>
      </div>
    </div>
  );
};

export default HomeNewsSlider;
