import React from 'react';

import './HomeNewsSliderItem.css';

const HomeNewsSliderItem = props => {
  return (
    <li className="HomeNewsSliderItem">
      <img src={props.image} alt=""/>
      <h5>{props.title}</h5>
      <p dangerouslySetInnerHTML={{__html: props.text}}/>
    </li>
  );
};

export default HomeNewsSliderItem;
