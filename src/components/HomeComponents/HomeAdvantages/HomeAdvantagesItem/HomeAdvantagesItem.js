import React from 'react';

import './HomeAdvantagesItem.css';

const HomeAdvantagesItem = ({imageBlack, title, text}) => {
  return (
    <div className="HomeAdvantagesItem">
      <div style={{background:`url(${imageBlack})`}} className="HomeAdvantagesItemImage"/>
      <div className="HomeAdvantagesItemText">
        <h5>{title}</h5>
        <p dangerouslySetInnerHTML={{__html: text}}/>
      </div>
    </div>
  );
};

export default HomeAdvantagesItem;
