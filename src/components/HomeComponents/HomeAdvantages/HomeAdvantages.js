import React from 'react';

import './HomeAdvantages.css';
import HomeAdvantagesItem from "./HomeAdvantagesItem/HomeAdvantagesItem";
import Container from "../../Container/Container";

const imagesUrl = 'http://jannat.sunrisetest.site';

const HomeAdvantages = ({title, items}) => {
  return (
    <div className="HomeAdvantages">
      <Container>
        <h2>{title}</h2>
        <div className="HomeAdvantagesItems">
          {items.map((item, index) => (
            <HomeAdvantagesItem
              imageBlack={imagesUrl + item.image}
              title={item.title}
              text={item.description}
              key={index}
            />
          ))}
        </div>
      </Container>
    </div>
  );
};

export default HomeAdvantages;
