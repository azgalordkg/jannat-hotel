import React from 'react';
import HomeFooterTextColumn from "../HomeFooterTextColumn/HomeFooterTextColumn";

import './HomeFooterText.css';

const HomeFooterText = props => {
  return (
    <div className="HomeFooterText">
      {props.columns.map((column, index) => (
        <HomeFooterTextColumn
          key={index}
          titles={props.titles[index]}
          text={column}
        />
      ))}
    </div>
  );
};

export default HomeFooterText;
