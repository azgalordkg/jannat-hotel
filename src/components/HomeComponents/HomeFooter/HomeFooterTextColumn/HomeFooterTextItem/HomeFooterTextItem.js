import React from 'react';

import './HomeFooterTextItem.css';

const HomeFooterTextItem = props => {
  return (
    <div className="HomeFooterTextItem">
      <h4>{props.title}</h4>
      <p dangerouslySetInnerHTML={{__html: props.text}}/>
    </div>
  );
};

export default HomeFooterTextItem;
