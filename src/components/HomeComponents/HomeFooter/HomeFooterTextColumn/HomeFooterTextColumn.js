import React from 'react';
import HomeFooterTextItem from "./HomeFooterTextItem/HomeFooterTextItem";

import './HomeFooterTextColumn.css';

const HomeFooterTextColumn = props => {
  return (
    <div className="HomeFooterTextColumn">
      {Object.values(props.text).map((textItem, index) => {
        return (
          <HomeFooterTextItem
            title={props.titles[index]}
            text={textItem}
            key={index}
          />
        )
      })}
    </div>
  );
};

export default HomeFooterTextColumn;
