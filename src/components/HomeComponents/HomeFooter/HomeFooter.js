import React from 'react';
import HomeFooterText from "./HomeFooterText/HomeFooterText";
import {Link} from 'react-scroll';

import './HomeFooter.css';

const HomeFooter = props => {
  return (
    <footer className="HomeFooter">
      <div className="HomeFooterLogoPart">
        <div className="HomeFooterLogo">
          <Link to="top" spy={true} smooth={true} duration={500}/>
        </div>
      </div>
      <div className="HomeFooterTextPart">
        <HomeFooterText
          columns={props.columns}
          titles={props.titles}
        />
        <div className="HomeFooterDescription">
          <span>©2017. Сеть 5* отелей "Jannat". Официальный сайт.</span>
        </div>
      </div>
    </footer>
  );
};

export default HomeFooter;
