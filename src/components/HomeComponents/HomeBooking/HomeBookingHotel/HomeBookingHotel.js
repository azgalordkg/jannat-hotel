import React from 'react';
import FormControl from "@material-ui/core/FormControl/FormControl";
import Select from "@material-ui/core/Select/Select";
import Input from "@material-ui/core/Input/Input";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";

import './HomeBookingHotel.css';

const HomeBookingHotel = (props) => {
  return (
    <div className="HomeBookingItem HomeBookingHotel">
      <FormControl className="">
        <label htmlFor="hotelLabel">Отель</label>
        <Select
          value={props.selectValue}
          onChange={props.selectChange}
          input={<Input name="hotel" id="hotelLabel" />}
          displayEmpty
          name="age"
          className=""
        >
          <MenuItem value="">
            <em>Выберите отель</em>
          </MenuItem>
          <MenuItem value="regency">Jannat Regency</MenuItem>
          <MenuItem value="resort">Jannat Resort</MenuItem>
        </Select>
      </FormControl>
    </div>
  );
};

export default HomeBookingHotel;
