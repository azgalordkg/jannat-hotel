import React from 'react';
import HomeBookingHotel from "./HomeBookingHotel/HomeBookingHotel";
import HomeBookingDatePicker from './HomeBookingDatePicker/HomeBookingDatePicker';

import './HomeBooking.css';
import HomeBookingGuests from "./HomeBookingGuests/HomeBookingGuests";

const HomeBooking = props => {
  return (
    <div className="HomeBooking">
      <div className="HomeBookingItem HomeBookingTitle">
        <h4>Бронирование номеров</h4>
      </div>
      <HomeBookingHotel
        selectValue={props.selectValue}
        selectChange={props.selectChange}
      />
      <HomeBookingDatePicker
        selected={props.dateStartSelected}
        onChange={props.dateStartChanged}
        title="Заезд"
      />
      <HomeBookingDatePicker
        selected={props.dateEndSelected}
        onChange={props.dateEndChanged}
        title="Отъезд"
      />
      <HomeBookingGuests
        counter={props.guests}
        increment={props.increment}
        decrement={props.decrement}
      />
      <div className="HomeBookingItem HomeBookingButton">
        <button>Найти номер</button>
      </div>
    </div>
  );
};

export default HomeBooking;
