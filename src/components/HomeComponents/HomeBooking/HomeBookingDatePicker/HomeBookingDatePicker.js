import React from "react";
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";
import './HomeBookingDatePicker.css';

const HomeBookingDatePicker = (props) => {
  return (
    <div className="HomeBookingItem HomeBookingDatePicker">
      <label>{props.title}</label>
      <DatePicker
        selected={props.selected}
        onChange={props.onChange}
        dateFormat="dd.MM.yyyy"
      />
    </div>
  );
};

export default HomeBookingDatePicker;
