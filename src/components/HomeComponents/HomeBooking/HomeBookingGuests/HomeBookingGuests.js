import React from 'react';

import './HomeBookingGuests.css';

const HomeBookingGuests = ({counter, increment, decrement}) => {
  return (
    <div className="HomeBookingItem HomeBookingGuests">
      <label>Гостей</label>
      <div className="HomeBookingGuestsCounter">
        <button onClick={increment} className="CounterButton">+</button>
        <span>{counter}</span>
        <button onClick={decrement} className="CounterButton">-</button>
      </div>
    </div>
  );
};

export default HomeBookingGuests;
