import React from 'react';
import HomeNavBar from "./HomeNavBar/HomeNavBar";

import './HomeHeader.css';

const HomeHeader = ({links}) => {
  return (
    <header className="HomeHeader">
      <HomeNavBar
        links={links}
      />
    </header>
  );
};

export default HomeHeader;
