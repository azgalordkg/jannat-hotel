import React from 'react';
import {NavLink} from 'react-router-dom';

import './HomeNavItem.css';

const HomeNavItem = ({link, exact, text, background, logo}) => {
  return (
    <li className="HomeNavItem">
      <div style={{backgroundImage: `url(${background})`}}>
        <NavLink to={link} exact={exact}/>
        <div style={{backgroundImage: `url(${logo})`}} className="HomeNavTitle"/>
        <p className="HomeNavText">{text}</p>
      </div>
    </li>
  );
};

export default HomeNavItem;
