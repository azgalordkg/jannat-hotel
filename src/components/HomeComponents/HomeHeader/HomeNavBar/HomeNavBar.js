import React from 'react';
import HomeNavItem from "../HomeNavItem/HomeNavItem";

import './HomeNavBar.css';

const URLS = ['/', '/regency','/resort', ''];
const imagesUrl = 'http://jannat.sunrisetest.site';

const HomeNavBar = ({links}) => {
  return (
    <nav className="HomeNavBar">
      <ul>
        {Object.values(links.map((link, index) => (
          <HomeNavItem
            link={URLS[index]}
            exact
            key={index}
            text={link.title}
            background={imagesUrl + link.cover}
            logo={imagesUrl + link.logo}
          />
        )))}
      </ul>
    </nav>
  );
};

export default HomeNavBar;
