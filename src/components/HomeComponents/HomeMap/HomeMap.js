import React, {Component} from 'react';
import {GoogleApiWrapper} from 'google-maps-react';

import './HomeMap.css';
import {googleStyles} from "../../../googleStyles";

import Manas from '../../../assets/img/svg/manas.svg';
import RegencyMap from '../../../assets/img/svg/regency-map.svg';
import ResortMap from '../../../assets/img/svg/resort-map.svg';

import './HomeMap.css';

const markers = [
  {
    coords: {lat: 43.054237, lng: 74.469069},
    icon: Manas,
  },
  {
    coords: {lat: 42.818689, lng: 74.619321},
    icon: RegencyMap,
  },
  {
    coords: {lat: 42.666014, lng: 74.684017},
    icon: ResortMap,
  }
];

const API_KEY = "AIzaSyBcA7WFDydKiMgV0Ppikxr__tGdZoWTNxk";

class HomeMap extends Component{
  state = {
    visits: [],
    isGoogleApiLoaded: window.google !== undefined && window.google !== null,
    markers: [],
    route: null,
  };

  componentDidMount() {
    if (window.google) {
      this.setState({isGoogleApiLoaded: true});
      this.loadMap(window.google);
      this.placeMarkerOnMap(markers);
    }

    if (!this.state.isGoogleApiLoaded) {
      let int = setInterval(() => {
        if (window.google) {
          this.setState({isGoogleApiLoaded: true});
          this.loadMap(window.google);
          this.placeMarkerOnMap(markers);
          clearInterval(int);
        }
      }, 2000);
    }

  };

  loadMap = google => {
    this._map = new google.maps.Map(document.getElementById("map"), {
      center: {lat: 42.874308, lng: 74.571416},
      zoom: 10,
      myTypeId: google.maps.MapTypeId.TERRAIN,
      styles: googleStyles,
    });
    this.calcRoute();
  };

  calcRoute = () => {
      const directionsService = new window.google.maps.DirectionsService();
      const directionsDisplay = new window.google.maps.DirectionsRenderer();
      const manas = new window.google.maps.LatLng(43.054237, 74.469069);
      const regency = new window.google.maps.LatLng(42.818689, 74.619321);
      const resort = new window.google.maps.LatLng(42.666014, 74.684017);

      const request = {
        origin: manas,
        destination: regency,
        travelMode: 'DRIVING'
      };
      directionsService.route(request, (response, status) => {
        if (status === 'OK') {
          directionsDisplay.setDirections(response);
        }
      });
  };

  placeMarkerOnMap = locations => {
    let marker;
    let markers = [];

    for (let i = 0; i < locations.length; i++) {
      marker = new window.google.maps.Marker({
        position: {lat: locations[i].coords.lat, lng:  locations[i].coords.lng},
        map: this._map,
        icon: locations[i].icon,
      });

      markers.push({marker: marker});
    }



    let route = new window.google.maps.Polyline({
      path: locations.map(item => item.coords),
      geodesic: true,
      strokeColor: '#BB986F',
      strokeOpacity: 1.0,
      strokeWeight: 6,
      travelMode: 'DRIVING'
    });

    route.setMap(this._map);
    // this.setState({
    //   markers: markers,
    // });
  };

  render() {
    return (
      <div className="HomeMap" id="map"/>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: API_KEY
})(HomeMap);
