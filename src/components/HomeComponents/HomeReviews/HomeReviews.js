import React from 'react';
import Container from "../../Container/Container";
import HomeReviewsLink from "./HomeReviewsLink/HomeReviewsLink";

import './HomeReviews.css';

const HomeReviews = ({title, links, titleLogo}) => {
  return (
    <div className="HomeReviews">
      <Container>
        <div className="HomeReviewsTitle">
          <h2>{title}</h2>
          <img src={titleLogo} alt=""/>
        </div>
        <div className="HomeReviewsLinks">
          {links.map((link, index) => (
            <HomeReviewsLink
              url={link.link}
              image={link.logo}
              key={index}
            />
          ))}
        </div>
      </Container>
    </div>
  );
};

export default HomeReviews;
