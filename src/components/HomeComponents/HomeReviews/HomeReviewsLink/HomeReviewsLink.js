import React from 'react';

import './HomeReviewsLink.css';

const imagesUrl = 'http://jannat.sunrisetest.site';

const HomeReviewsLink = ({url, image}) => {
  return (
    <div className="HomeReviewsLink">
      <a href={url} target="blank"> </a>
      <img src={imagesUrl + image} alt=""/>
    </div>
  );
};

export default HomeReviewsLink;
