import React from 'react';
import NavBar from "./NavBar/NavBar";
import NavBarItem from "./NavBarItem/NavBarItem";
import RegencyResortHeaderLangItem from "./RegencyResortHeaderLangItem/RegencyResortHeaderLangItem";

import './RegencyResortHeader.css';

const RegencyResortHeader = props => {
  return (
    <header className="RegencyResortHeader">
      <div style={{backgroundImage: `url(${props.mainLogo})`}} className="RegencyResortHeaderLogo"/>
      <NavBar>
        {props.navLinks.map(navLink => (
          <NavBarItem key={navLink.id} id={navLink.id}>{navLink.text}</NavBarItem>
        ))}
      </NavBar>
      <div className="RegencyResortHeaderLang">
        {props.languages.map(language => (
          <RegencyResortHeaderLangItem
            onClick={null}
            text={language.text}
            key={language.text}
          />
        ))}
      </div>
      <div className="RegencyResortHeaderTrip">
        <a href="https://google.com" target="blank"> </a>
      </div>
    </header>
  );
};

export default RegencyResortHeader;
