import React from 'react';
import { Link } from 'react-scroll';

import './NavBarItem.css'

const NavBarItem = props => {
  return (
    <li className="NavBarItem">
      <Link to={props.id}>{props.children}</Link>
    </li>
  );
};

export default NavBarItem;
