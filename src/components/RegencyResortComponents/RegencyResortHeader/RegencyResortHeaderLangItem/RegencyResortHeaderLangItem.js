import React from 'react';

import './RegencyResortHeaderLangItem.css';

const RegencyResortHeaderLangItem = ({onClick, text}) => {
  return (
    <button className="RegencyResortHeaderLangItem" onClick={onClick}>
      {text}
    </button>
  );
};

export default RegencyResortHeaderLangItem;
