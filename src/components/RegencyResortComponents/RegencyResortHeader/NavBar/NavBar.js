import React from 'react';

import './NavBar.css';

const NavBar = ({children}) => {
  return (
    <nav className="NavBar">
      <ul>
        {children}
      </ul>
    </nav>
  );
};

export default NavBar;
