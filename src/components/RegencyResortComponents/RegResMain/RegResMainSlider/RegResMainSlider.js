import React from 'react';

import './RegResMainSlider.css';

const imagesUrl = 'http://jannat.sunrisetest.site';

const RegResMainSlider = props => {
  return (
    <div className="RegResMainSlider">
      <div uk-slideshow="true">
        <ul className="uk-slideshow-items">
          {props.slideItems.map(slideItem => (
            <li key={slideItem.id}>
              <img src={imagesUrl + slideItem.image} alt="" uk-cover="true"/>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default RegResMainSlider;
