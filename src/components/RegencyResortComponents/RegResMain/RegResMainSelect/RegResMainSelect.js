import React from 'react';

import './RegResMainSelect.css';

const RegResMainSelect = props => {
  return (
    <div className="RegResMainSelect">
      <span className="RegResMainSelectTitle">{props.title}</span>
      <div className="RegResMainSelectItem">
        <button className="RegResMainSelectItemButton" onClick={props.onSelectClick}>
          {props.guestsInfo}
        </button>
        {props.ifDrop ? <div className="RegResMainSelectDrop">
          <div className="RegResMainSelectDropItems">
            items here
          </div>
          <div className="RegResMainSelectDropItemsBtn">
            <button onClick={props.onAddClick}>
              {props.addButtonText}
            </button>
          </div>
          <div className="RegResMainSelectDropItemsAccept">
            <button>{props.okClickText}</button>
            <button>{props.cancelClickText}</button>
          </div>
        </div> : null}

      </div>
    </div>
  );
};

export default RegResMainSelect;
