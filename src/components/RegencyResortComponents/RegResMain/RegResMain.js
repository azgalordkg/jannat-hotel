import React from 'react';
import RegResMainDatePicker from "./RegResMainDatePicker/RegResMainDatePicker";
import RegResMainSelect from "./RegResMainSelect/RegResMainSelect";

import './RegResMain.css';
import RegResMainSlider from "./RegResMainSlider/RegResMainSlider";

const RegResMain = props => {
  return (
    <div className="RegResMain">
      <RegResMainSlider
        slideItems={props.slideItems}
      />
      <div className="RegResMainBooking">
        <h2>Бронирование номеров</h2>
        <span>Забронируйте с 15% скидкой прямо сейчас!</span>
        <div className="RegResMainForm">
          <RegResMainDatePicker
            title={props.startTitle}
            selected={props.startSelected}
            onChange={props.startOnChange}
          />
          <RegResMainDatePicker
            title={props.endTitle}
            selected={props.endSelected}
            onChange={props.endOnChange}
          />
          <RegResMainSelect
            title={props.selectTitle}
            onSelectClick={props.onSelectClick}
            guestsInfo={props.guestsInfo}
            ifDrop={props.ifDrop}
            onAddClick={props.onAddClick}
            addButtonText={props.addButtonText}
            okClickText={props.okClickText}
            cancelClickText={props.cancelClickText}
          />
        </div>
      </div>
    </div>
  );
};

export default RegResMain;
