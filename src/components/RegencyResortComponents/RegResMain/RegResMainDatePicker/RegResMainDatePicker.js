import React from "react";
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";
import './RegResMainDatePicker.css';

const RegResMainDatePicker = (props) => {
  return (
    <div className="RegResMainDatePicker">
      <label>{props.title}</label>
      <DatePicker
        selected={props.selected}
        onChange={props.onChange}
        dateFormat="dd.MM.yyyy"
      />
    </div>
  );
};

export default RegResMainDatePicker;
