import React, {Fragment} from 'react';
import RegencyResortHeader from "../RegencyResortComponents/RegencyResortHeader/RegencyResortHeader";
import RegencyResortFooter from "../RegencyResortComponents/RegencyResortFooter/RegencyResortFooter";

import './Layout.css';

const Layout = props => {
  return (
    <Fragment>
      <RegencyResortHeader
        navLinks={props.navLinks}
        languages={props.languages}
        mainLogo={props.mainLogo}
      />
      <div className="LayoutContent">
        {props.children}
      </div>
      <RegencyResortFooter/>
    </Fragment>
  );
};

export default Layout;
