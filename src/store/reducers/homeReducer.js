import {GET_FAILURE, GET_REQUEST, GET_SUCCESS} from "../actions/actionTypes";

const initialState = {
  loading: false,
  error: null,
  contacts: null,
  banner: null,
  reasons: null,
  news: null,
  services: null,
  tripadvisor: null,
};

const homeReducer = (state = initialState, action) => {
  switch (action.type) {
    case (GET_REQUEST):
      return {
        ...state,
        loading: true,
      };
    case (GET_SUCCESS):
      return {
        ...state,
        loading: false,
        contacts: action.response.contacts,
        banner: action.response.banner,
        reasons: action.response.reasons,
        news: action.response.news,
        services: action.response.services,
        tripadvisor: action.response.tripadvisor,
      };
    case (GET_FAILURE):
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    default:
      return state
  }
};

export default homeReducer;
