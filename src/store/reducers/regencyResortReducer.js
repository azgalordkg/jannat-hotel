import {GET_REGENCYRESORT_FAILURE, GET_REGENCYRESORT_REQUEST, GET_REGENCYRESORT_SUCCESS} from "../actions/actionTypes";

const initialState = {
  error: null,
  loading: false,
  about_us: null,
  slider: null,
};

const regencyResortReducer = (state = initialState, action) => {
  switch (action.type) {
    case (GET_REGENCYRESORT_REQUEST):
      return {
        ...state,
        loading: true,
      };
    case (GET_REGENCYRESORT_SUCCESS):
      return {
        ...state,
        loading: false,
        about_us: action.response.about_us,
        slider: action.response.slider,
      };
    case (GET_REGENCYRESORT_FAILURE):
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
};

export default regencyResortReducer;
