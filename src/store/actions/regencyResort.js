import {GET_REGENCYRESORT_FAILURE, GET_REGENCYRESORT_REQUEST, GET_REGENCYRESORT_SUCCESS} from "./actionTypes";
import axios from '../../axios-jannat';

export const getRegencyResortRequest = () => ({type: GET_REGENCYRESORT_REQUEST});
export const getRegencyResortSuccess = response => ({type: GET_REGENCYRESORT_SUCCESS, response});
export const getRegencyResortFailure = error => ({type: GET_REGENCYRESORT_FAILURE, error});


export const fetchRegencyResortData = lang => {
  return dispatch => {
    dispatch(getRegencyResortRequest());

    axios.get('regency/').then(
      response => dispatch(getRegencyResortSuccess(response.data[lang])),
      error => dispatch(getRegencyResortFailure(error)),
    );
  }
};
