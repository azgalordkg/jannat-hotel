import {GET_FAILURE, GET_REQUEST, GET_SUCCESS} from "./actionTypes";
import axios from '../../axios-jannat';

export const getRequest = () => ({type: GET_REQUEST});
export const getSuccess = response => ({type: GET_SUCCESS, response});
export const getFailure = error => ({type: GET_FAILURE, error});

export const fetchHomeData = (lang) => {
  return dispatch => {
    dispatch(getRequest());

    axios.get('').then(
      response => dispatch(getSuccess(response.data[lang])),
      error => dispatch(getFailure(error)),
    )
  }
};
