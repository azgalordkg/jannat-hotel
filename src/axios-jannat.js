import axios from 'axios';

const instance = axios.create({
  baseURL: 'http://jannat.sunrisetest.site/',
});

export default instance;
